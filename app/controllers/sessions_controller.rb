require 'warehouse/prefetch_craftsmen'

class SessionsController < ApplicationController
  layout "sessions_layout"
  skip_before_action :authenticate_user!
  
  def oauth_signin
  end

end
